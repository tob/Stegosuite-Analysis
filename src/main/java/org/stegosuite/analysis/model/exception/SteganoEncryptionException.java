package org.stegosuite.analysis.model.exception;

public class SteganoEncryptionException
		extends SteganoExtractException {

	private static final long serialVersionUID = 1L;

	public SteganoEncryptionException(String message) {
		super(message);
	}

}
