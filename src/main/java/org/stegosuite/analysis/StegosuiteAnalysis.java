package org.stegosuite.analysis;

import org.slf4j.LoggerFactory;
import org.stegosuite.analysis.ui.cli.Cli;
import org.stegosuite.analysis.ui.gui.Gui;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;

public class StegosuiteAnalysis {

	public static void main(String[] args) {
		Logger root = (Logger) LoggerFactory.getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME);
		root.setLevel(Level.DEBUG);
		if (args.length == 0) {
			new Gui(null);
		} else if (args[0].startsWith("/")) {
			new Gui(args[0]);
		} else {
			new Cli(args);
		}
	}
}
