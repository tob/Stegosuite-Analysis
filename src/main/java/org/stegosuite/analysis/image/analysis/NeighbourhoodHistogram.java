package org.stegosuite.analysis.image.analysis;

import java.awt.Color;
import java.util.TreeSet;

import org.stegosuite.analysis.image.format.ImageFormat;

/**
 * Neighbourhood histogram attacks.
 *
 * Only works for BMP images.
 *
 * Sources: http://simple-steganalysis-suite.googlecode.com/svn/wiki/articles/
 * Reliable_Detection_of_LSB_Steganography_Based_on_the_Difference_Image_Histogram .pdf
 * https://github.com/Nesh108/simple-steganalysis-suite
 */
public class NeighbourhoodHistogram {

	/**
	 * Method to actual perform the attack.
	 *
	 * @param image The image to be analyzed
	 * @return The values of the histogram
	 */
	public static double[] attack(ImageFormat image) {
		// We use a TreeSet to speed up the attack.
		TreeSet<Node> colorTree = new TreeSet<>();
		int width = image.getWidth();
		int height = image.getHeight();
		double[] counts = new double[27]; // Up to 26 neighbour colours and the 0 is there.

		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				Color actualColor = new Color(image.getBufferedImage().getRGB(x, y));
				colorTree.add(new Node(actualColor.getRed(), actualColor.getGreen(), actualColor.getBlue()));
			}
		}

		int count = 0;
		for (Node n : colorTree) {
			if (colorTree.contains(new Node(n.r - 1, n.g - 1, n.b - 1))) {
				count++;
			}
			if (colorTree.contains(new Node(n.r - 1, n.g - 1, n.b))) {
				count++;
			}
			if (colorTree.contains(new Node(n.r - 1, n.g - 1, n.b + 1))) {
				count++;
			}
			if (colorTree.contains(new Node(n.r - 1, n.g, n.b - 1))) {
				count++;
			}
			if (colorTree.contains(new Node(n.r - 1, n.g, n.b))) {
				count++;
			}
			if (colorTree.contains(new Node(n.r - 1, n.g, n.b + 1))) {
				count++;
			}
			if (colorTree.contains(new Node(n.r - 1, n.g + 1, n.b - 1))) {
				count++;
			}
			if (colorTree.contains(new Node(n.r - 1, n.g + 1, n.b))) {
				count++;
			}
			if (colorTree.contains(new Node(n.r - 1, n.g + 1, n.b + 1))) {
				count++;
			}

			if (colorTree.contains(new Node(n.r, n.g - 1, n.b - 1))) {
				count++;
			}
			if (colorTree.contains(new Node(n.r, n.g - 1, n.b))) {
				count++;
			}
			if (colorTree.contains(new Node(n.r, n.g - 1, n.b + 1))) {
				count++;
			}
			if (colorTree.contains(new Node(n.r, n.g, n.b - 1))) {
				count++;
			}
			if (colorTree.contains(new Node(n.r, n.g, n.b + 1))) {
				count++;
			}
			if (colorTree.contains(new Node(n.r, n.g + 1, n.b - 1))) {
				count++;
			}
			if (colorTree.contains(new Node(n.r, n.g + 1, n.b))) {
				count++;
			}
			if (colorTree.contains(new Node(n.r, n.g + 1, n.b + 1))) {
				count++;
			}

			if (colorTree.contains(new Node(n.r + 1, n.g - 1, n.b - 1))) {
				count++;
			}
			if (colorTree.contains(new Node(n.r + 1, n.g - 1, n.b))) {
				count++;
			}
			if (colorTree.contains(new Node(n.r + 1, n.g - 1, n.b + 1))) {
				count++;
			}
			if (colorTree.contains(new Node(n.r + 1, n.g, n.b - 1))) {
				count++;
			}
			if (colorTree.contains(new Node(n.r + 1, n.g, n.b))) {
				count++;
			}
			if (colorTree.contains(new Node(n.r + 1, n.g, n.b + 1))) {
				count++;
			}
			if (colorTree.contains(new Node(n.r + 1, n.g + 1, n.b - 1))) {
				count++;
			}
			if (colorTree.contains(new Node(n.r + 1, n.g + 1, n.b))) {
				count++;
			}
			if (colorTree.contains(new Node(n.r + 1, n.g + 1, n.b + 1))) {
				count++;
			}
			counts[count]++;
			count = 0;

		}
		return counts;
	}

	/**
	 * Get the mean value of the values in the histogram.
	 *
	 * @param y Values of the histogram
	 * @return Mean of the values contained in {@code y}
	 */
	public static double getMean(double y[]) {
		double average = 0;
		double total = 0;
		for (int i = 0; i < y.length; i++) {
			average += i * y[i];
			total += y[i];
		}
		return (average / total);
	}

	/**
	 * Inner class to represent a node of the tree set used in the attack.
	 *
	 */
	private static class Node
			implements Comparable<Node> {

		// Attributes of the class
		int r;
		int g;
		int b;

		/**
		 * Constructor of the object Node.
		 *
		 * @param red Red value represented as {@code int}.
		 * @param g Red value represented as {@code int}.
		 * @param b Blue value represented as {@code int}
		 */
		public Node(int red, int green, int blue) {
			r = red;
			g = green;
			b = blue;
		}

		@Override
		public int compareTo(Node n) {
			if (r != n.r) {
				return r - n.r;
			} else if (g != n.g) {
				return g - n.g;
			} else if (b != n.b) {
				return b - n.b;
			}
			return 0;
		}
	}
}
