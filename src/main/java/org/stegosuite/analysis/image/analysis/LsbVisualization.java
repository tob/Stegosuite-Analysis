package org.stegosuite.analysis.image.analysis;

import java.awt.Color;
import java.awt.image.BufferedImage;

import org.stegosuite.analysis.image.format.ImageFormat;
import org.stegosuite.analysis.image.util.ByteUtils;
import org.stegosuite.analysis.image.util.RgbChannel;

public class LsbVisualization {

	public static ImageFormat calculate(ImageFormat image) {
		ImageFormat visualizedImage = image.clone(BufferedImage.TYPE_INT_ARGB);

		for (int y = 0; y < visualizedImage.getHeight(); y++) {
			for (int x = 0; x < visualizedImage.getWidth(); x++) {
				Color color = new Color(visualizedImage.getBufferedImage().getRGB(x, y));
				for (RgbChannel channel : RgbChannel.RGB()) {
					color = channel.setValue(color, ByteUtils.isOdd(channel.getValue(color)) ? 255 : 0);
				}
				visualizedImage.getBufferedImage().setRGB(x, y, color.getRGB());
			}
		}

		return visualizedImage;
	}
}
