package org.stegosuite.analysis.image.analysis;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import org.stegosuite.analysis.image.format.GIFImage;
import org.stegosuite.analysis.image.format.ImageFormat;
import org.stegosuite.analysis.image.util.ColorDistance;

/**
 * Visualizing the LSB (least significant bit) of the indices of the sorted color table of
 * GIF-Images.
 *
 * described in : Andreas Westfeld and Andreas Pfitzmann. "Attacks on steganographic systems."
 * Information Hiding. Springer Berlin Heidelberg, 2000.
 */
public class SortedColorTableAttack {

	public static ImageFormat calculate(ImageFormat image) {
		GIFImage gifImage = (GIFImage) image.clone();
		List<Color> table = gifImage.getColorTable();
		List<Color> sortedTable = gifImage.getSortedColorTable(ColorDistance.CIEDE_2000);
		List<Color> sortedTable2 = new ArrayList<>(sortedTable);
		List<Color> restoredTable = new ArrayList<>();

		for (int i = 0; i < sortedTable2.size(); i++) {
			sortedTable2.set(i, i % 2 == 0 ? Color.BLACK : Color.WHITE);
		}

		for (Color color : table) {
			int index = sortedTable.indexOf(color);
			restoredTable.add(sortedTable2.get(index));
		}

		gifImage.setColorTable(restoredTable);

		return gifImage;
	}
}
