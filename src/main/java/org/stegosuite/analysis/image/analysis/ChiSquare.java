package org.stegosuite.analysis.image.analysis;

import java.awt.Color;

import org.apache.commons.math3.stat.inference.ChiSquareTest;
import org.stegosuite.analysis.image.format.ImageFormat;

/**
 *
 * Chi-Square attack, described in : Attacks on Steganographic Systems Andreas Westfeld and Andreas
 * Pfitzmann
 *
 * Only works for sequential embedding or very high (>99%) embedding rates.
 *
 * Sources: https://github.com/b3dk7/StegExpose https://github.com/Nesh108/simple-steganalysis-suite
 *
 * More info: http://www.guillermito2.net/stegano/tools/index.html http://cuneytcaliskan
 * .blogspot.fr/2011/12/steganalysis-chi-square-attack-lsb.html
 *
 */
public class ChiSquare {

	public static double[] chiSquareAttackTopToBottom(ImageFormat image) {

		int csSize = 1024;

		int nbBlocks = ((3 * image.getWidth() * image.getHeight()) / csSize) - 1;
		double[] x = new double[nbBlocks];
		double[] chi = new double[nbBlocks];

		int width = image.getWidth();
		int height = image.getHeight();
		int block = 0;
		int nbBytes = 1;
		int[] values = new int[256];
		double[] expectedValues = new double[128];
		long[] pov = new long[128];

		for (int i = 0; i < values.length; i++) {
			values[i] = 1;
			x[i] = i;
		}

		for (int j = 0; j < height; j++) {
			for (int i = 0; i < width; i++) {
				Color color = new Color(image.getBufferedImage().getRGB(i, j));
				if (block < chi.length) {
					values[color.getRed()]++;
					nbBytes++;
					if (nbBytes > csSize) {
						for (int k = 0; k < expectedValues.length; k++) {
							expectedValues[k] = (values[2 * k] + values[2 * k + 1]) / (double) 2;
							pov[k] = values[2 * k];
						}
						chi[block] = new ChiSquareTest().chiSquareTest(expectedValues, pov);
						block++;
						nbBytes = 1;
						// for(int m=0; m<values.length; m++) { values[m] = 1; }
					}
				}

				if (block < chi.length) {
					values[color.getGreen()]++;
					nbBytes++;
					if (nbBytes > csSize) {
						for (int k = 0; k < expectedValues.length; k++) {
							expectedValues[k] = (values[2 * k] + values[2 * k + 1]) / (double) 2;
							pov[k] = values[2 * k];
						}
						chi[block] = new ChiSquareTest().chiSquareTest(expectedValues, pov);
						block++;
						nbBytes = 1;
						// for(int m=0; m<values.length; m++) { values[m] = 1; }
					}
				}

				if (block < chi.length) {
					values[color.getBlue()]++;
					nbBytes++;
					if (nbBytes > csSize) {
						for (int k = 0; k < expectedValues.length; k++) {
							expectedValues[k] = (values[2 * k] + values[2 * k + 1]) / 2;
							pov[k] = values[2 * k];
						}
						chi[block] = new ChiSquareTest().chiSquareTest(expectedValues, pov);
						block++;
						nbBytes = 1;
						// for(int m=0; m<values.length; m++) { values[m] = 1; }
					}
				}
			}
		}

		// double csQuant = 0;
		// for (double csVal : chi)
		// csQuant += csVal;
		// double cs = Math.min(Math.abs(csQuant / chi.length), 1); // chi-square-propability

		// make zeros visible in the diagram
		for (int i = 0; i < chi.length; i++) {
			if (chi[i] == 0) {
				chi[i] += 0.001;

			}
		}
		return chi;
	}

}
