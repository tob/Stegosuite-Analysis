package org.stegosuite.analysis.image.analysis;

import java.awt.Color;

import org.stegosuite.analysis.image.format.ImageFormat;

/**
 * Difference image histogram attack
 *
 * Sources: http://simple-steganalysis-suite.googlecode.com/svn/wiki/articles/
 * Reliable_Detection_of_LSB_Steganography_Based_on_the_Difference_Image_Histogram.pdf
 * https://github.com/Nesh108/simple-steganalysis-suite
 */
public class PixelsDifferenceHistogram {

	/**
	 * Method to actual perform the attack.
	 *
	 * @param image Image to be analyzed.
	 * @return The values of the histogram
	 */
	public static double[] attack(ImageFormat image) {
		int height = image.getHeight();
		int width = image.getWidth();
		double[] values = new double[511]; // 255 positives, 255 negatives and 0

		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width && (x + 1) < width; x += 2) {
				Color color = new Color(image.getBufferedImage().getRGB(x, y));
				Color colorPlusOne = new Color(image.getBufferedImage().getRGB(x + 1, y));
				;

				// Add 255 because of the negative values
				values[color.getRed() - colorPlusOne.getRed() + 255]++;
				values[color.getGreen() - colorPlusOne.getGreen() + 255]++;
				values[color.getBlue() - colorPlusOne.getBlue() + 255]++;
			}
		}

		return values;
	}
}
