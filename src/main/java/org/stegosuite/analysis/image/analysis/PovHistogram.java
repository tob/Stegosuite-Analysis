package org.stegosuite.analysis.image.analysis;

import org.stegosuite.analysis.image.format.GIFImage;
import org.stegosuite.analysis.image.format.ImageFormat;
import org.stegosuite.analysis.image.util.ColorDistance;

/**
 * Pairs of values histogram attack, as presented in the first lecture of the project.
 */
public class PovHistogram {

	// Attributes of the class
	private static double[] lsbZero = new double[8];
	private static double[] lsbOne = new double[8];
	private static double[] means = new double[8];

	/**
	 * Method to actual perform the attack and fill the attributes of the class with the computed
	 * values.
	 *
	 * @param image The image to be analyzed.
	 */
	public static void attack(ImageFormat image) {
		ImageFormat clonedImage = image.clone();

		// If the image is a GIF the palette is sorted before the analysis
		if (clonedImage.getClass().equals(GIFImage.class)) {
			GIFImage gifImage = (GIFImage) image;
			gifImage.setColorTable(gifImage.getSortedColorTable(ColorDistance.CIEDE_2000));
		}

		int count0000 = 0;
		int count0001 = 0;
		int count0010 = 0;
		int count0011 = 0;
		int count0100 = 0;
		int count0101 = 0;
		int count0110 = 0;
		int count0111 = 0;
		int count1000 = 0;
		int count1001 = 0;
		int count1010 = 0;
		int count1011 = 0;
		int count1100 = 0;
		int count1101 = 0;
		int count1110 = 0;
		int count1111 = 0;

		// Can this be done without converting to ImageData?
		byte[] pixelsReferences = image.getImageData().data;

		for (byte pixelsReference : pixelsReferences) {
			switch (pixelsReference & 0b00001111) {

				case 0:
					count0000++;
					break;
				case 1:
					count0001++;
					break;
				case 2:
					count0010++;
					break;
				case 3:
					count0011++;
					break;
				case 4:
					count0100++;
					break;
				case 5:
					count0101++;
					break;
				case 6:
					count0110++;
					break;
				case 7:
					count0111++;
					break;
				case 8:
					count1000++;
					break;
				case 9:
					count1001++;
					break;
				case 10:
					count1010++;
					break;
				case 11:
					count1011++;
					break;
				case 12:
					count1100++;
					break;
				case 13:
					count1101++;
					break;
				case 14:
					count1110++;
					break;
				case 15:
					count1111++;
					break;
				default:
					break;
			}
		}

		means[0] = computeMean(count0000, count0001);
		means[1] = computeMean(count0010, count0011);
		means[2] = computeMean(count0100, count0101);
		means[3] = computeMean(count0110, count0111);
		means[4] = computeMean(count1000, count1001);
		means[5] = computeMean(count1010, count1011);
		means[6] = computeMean(count1100, count1101);
		means[7] = computeMean(count1110, count1111);

		lsbZero[0] = count0000;
		lsbZero[1] = count0010;
		lsbZero[2] = count0100;
		lsbZero[3] = count0110;
		lsbZero[4] = count1000;
		lsbZero[5] = count1010;
		lsbZero[6] = count1100;
		lsbZero[7] = count1110;

		lsbOne[0] = count0001;
		lsbOne[1] = count0011;
		lsbOne[2] = count0101;
		lsbOne[3] = count0111;
		lsbOne[4] = count1001;
		lsbOne[5] = count1011;
		lsbOne[6] = count1101;
		lsbOne[7] = count1111;

	}

	/**
	 * Get the amount of values with LSB = 0.
	 *
	 * @return Table with the amount of values with LSB = 0
	 */
	public static double[] getLsbZero() {
		return lsbZero;
	}

	/**
	 * Get the amount of values with LSB = 1.
	 *
	 * @return Table with the amount of values with LSB = 1.
	 */
	public static double[] getLsbOne() {
		return lsbOne;
	}

	/**
	 * Get the means between the values with LSB = 0 and LSB = 1.
	 *
	 * @return Table with the means between the values with LSB = 0 and LSB = 1.
	 */
	public static double[] getMeans() {
		return means;
	}

	/**
	 * Compute the mean between two {@code int}.
	 *
	 * @param n First number.
	 * @param m Second number.
	 * @return Mean between the two numbers.
	 */
	private static double computeMean(int n, int m) {
		return (n + m) / 2;
	}
}
