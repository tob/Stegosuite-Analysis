package org.stegosuite.analysis.image.format;

import java.io.File;

import org.stegosuite.analysis.model.exception.SteganoImageException;

public class JPGImage
		extends ImageFormat {

	public static final String FILE_EXTENSION = "jpg";

	@Override
	public String getFileExtension() {
		return FILE_EXTENSION;
	}

	@Override
	public void save(File file)
			throws SteganoImageException {
		// temporally nothing because it's already done in embedding
	}

}
