package org.stegosuite.analysis.ui.cli;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.beust.jcommander.JCommander;

import ch.qos.logback.classic.Level;

public class Cli {

	private String[] args;
	private Arguments arguments;
	private static final Logger LOG = LoggerFactory.getLogger(Cli.class);

	public Cli(String[] s) {
		args = s;
		parseCli();
	}

	private void parseCli() {
		arguments = new Arguments();
		JCommander jCommander = new JCommander(arguments, args);
		if (arguments.isVerbose()) {
			ch.qos.logback.classic.Logger rootLogger = (ch.qos.logback.classic.Logger) LoggerFactory
					.getLogger(Logger.ROOT_LOGGER_NAME);
			rootLogger.setLevel(Level.DEBUG);
		}
		if (arguments.isEmbed()) {
			// embed();
		} else if (arguments.isExtract()) {
			// extract();
		} else if (arguments.isHelp()) {
			jCommander.usage();
			return;
		}
	}
}
