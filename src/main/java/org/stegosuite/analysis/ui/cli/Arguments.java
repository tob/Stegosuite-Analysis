package org.stegosuite.analysis.ui.cli;

import com.beust.jcommander.Parameter;

public class Arguments {

	@Parameter(names = { "-h", "--help" }, help = true, description = "Displays this message.")
	private boolean help = false;

	@Parameter(names = { "-V", "--verbose" }, description = "Show more information.")
	private boolean verbose = false;

	@Parameter(names = {
			"--disable-noise-detection" }, description = "Disables the automatic exclusion of homogeneous areas in the image.")
	private boolean disableNoiseDetection = false;

	@Parameter(names = "-e", description = "Embed data into image")
	private boolean embed = false;

	@Parameter(names = "-x", description = "Extract data from image")
	private boolean extract = false;

	@Parameter(names = "-i", description = "Image for embedding/extracting")
	private String steganogramPath;

	@Parameter(names = "-k", description = "Secret key used for encrytion and embedding/extracting")
	private String secretkey;

	@Parameter(names = { "-m", "--embedMessage" }, description = "Message to embed")
	private String message = "This is the default secret message";

	@Parameter(names = { "-f", "--embedFile" }, description = "File to embed")
	private String filePath;

	boolean isDisableNoiseDetection() {
		return disableNoiseDetection;
	}

	boolean isHelp() {
		return help;
	}

	boolean isVerbose() {
		return verbose;
	}

	boolean isEmbed() {
		return embed;
	}

	boolean isExtract() {
		return extract;
	}

	String getSteganogramPath() {
		return steganogramPath;
	}

	String getFilePath() {
		return filePath;
	}

	String getSecretKey() {
		return secretkey;
	}

	String getMessage() {
		return message;
	}

}
