package org.stegosuite.analysis.ui.gui;

import java.util.ResourceBundle;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

/**
 * Contains methods for creating the GUI-elements and setting their default parameters.
 */
public class GuiComponents {

	final static byte LOAD_MENU_ITEM = 0;
	// EmbedUi embedUi;
	// ExtractUi extractUi;
	AnalyzeUi analyzeUi;
	private final ResourceBundle L = ResourceBundle.getBundle("Messages");

	Shell createShell(Display display) {
		Shell shell = new Shell(display);
		shell.setText("StegoSuiteAnalysis");
		// Image icon = new Image(display,
		// this.getClass().getClassLoader().getResourceAsStream("man-hat.png"));
		// shell.setImage(icon);
		// shell.setImage(new Image(display, "resources/images/man-hat.png"));
		shell.setMinimumSize(400, 320);
		shell.setSize(600, 350);
		shell.setLayout(new FormLayout());
		return shell;
	}

	Label createStatusBar(Shell shell) {
		Label label = new Label(shell, SWT.SHADOW_NONE);
		FormData labelData = new FormData();
		labelData.left = new FormAttachment(0, 5);
		labelData.right = new FormAttachment(100);
		labelData.bottom = new FormAttachment(100, -1);
		label.setLayoutData(labelData);
		return label;
	}

	Menu createMenuBar(Shell shell) {
		Menu menuBar = new Menu(shell, SWT.BAR);
		MenuItem cascadeFileMenu = new MenuItem(menuBar, SWT.CASCADE);
		cascadeFileMenu.setText(L.getString("file_menu"));

		Menu fileMenu = new Menu(shell, SWT.DROP_DOWN);
		cascadeFileMenu.setMenu(fileMenu);

		MenuItem loadItem = new MenuItem(fileMenu, SWT.PUSH, LOAD_MENU_ITEM);
		loadItem.setText(L.getString("load_image_menu"));

		shell.setMenuBar(menuBar);
		return menuBar;
	}

	void createLayout(Shell parent, Control below) {
		final Composite composite = new Composite(parent, SWT.NONE);
		final FormData formData = new FormData();
		formData.left = new FormAttachment(0);
		formData.right = new FormAttachment(100);
		formData.top = new FormAttachment(0);
		formData.bottom = new FormAttachment(below);
		composite.setLayoutData(formData);

		analyzeUi = new AnalyzeUi(composite, this);
	}

	TabFolder createTabFolder(Composite parent, Control below) {
		final TabFolder tabFolder = new TabFolder(parent, SWT.NONE);
		final FormData tabData = new FormData();
		tabData.left = new FormAttachment(0);
		tabData.right = new FormAttachment(100);
		tabData.top = new FormAttachment(0);
		tabData.bottom = new FormAttachment(below);
		tabFolder.setLayoutData(tabData);

		// embedUi = new EmbedUi(tabFolder, this);
		// extractUi = new ExtractUi(tabFolder, this);
		analyzeUi = new AnalyzeUi(tabFolder, this);

		return tabFolder;
	}

	Composite createControlsComposite(Composite parent) {
		Composite compositeControls = new Composite(parent, SWT.NONE);

		GridLayout mGridLayout = new GridLayout(2, true);
		mGridLayout.verticalSpacing = 18;
		mGridLayout.marginTop = 12;
		mGridLayout.marginLeft = 12;
		mGridLayout.makeColumnsEqualWidth = false;
		compositeControls.setLayout(mGridLayout);

		StyledText text = new StyledText(compositeControls, SWT.MULTI | SWT.V_SCROLL | SWT.WRAP | SWT.BORDER);
		text.setAlwaysShowScrollBars(false);
		text.setText(L.getString("message_text"));
		text.setToolTipText(L.getString("message_text_tooltip"));
		text.setForeground(Display.getDefault().getSystemColor(SWT.COLOR_DARK_GRAY));

		text.addListener(SWT.MouseDown, event -> {
			if (text.getData() == null) {
				text.setData(1);
				text.setForeground(null);
				text.setText("");
			}
		});

		text.addListener(SWT.FocusOut, event -> {
			if (text.getText().isEmpty()) {
				text.setText(L.getString("message_text"));
				text.setForeground(Display.getDefault().getSystemColor(SWT.COLOR_DARK_GRAY));
				text.setData(null);
			}
		});

		/*
		 * text.addListener(SWT.MouseDown, event -> { text.setText(""); });
		 */

		GridData data = new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1);
		data.heightHint = 70;
		text.setLayoutData(data);

		return compositeControls;
	}

	Composite createFileEmbedding(Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);
		final GridData gridData2Columns = new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1);
		// gridData2Columns.heightHint = 100;
		composite.setLayoutData(gridData2Columns);

		GridLayout mGridLayout = new GridLayout(2, true);
		mGridLayout.verticalSpacing = 3;
		mGridLayout.marginLeft = 0;
		mGridLayout.marginRight = 0;
		mGridLayout.marginTop = 0;
		mGridLayout.horizontalSpacing = 0;
		mGridLayout.marginWidth = 0;
		composite.setLayout(mGridLayout);

		Label l = new Label(composite, SWT.NONE);
		Label l2 = new Label(composite, SWT.NONE);
		GridData gd = new GridData();
		gd.horizontalAlignment = SWT.END;
		l2.setLayoutData(gd);

		l2.setForeground(Display.getDefault().getSystemColor(SWT.COLOR_DARK_GRAY));

		final Table table = new Table(composite, SWT.SINGLE | SWT.BORDER);
		final GridData gridData2Columns2 = new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1);
		gridData2Columns2.heightHint = 50;

		table.setLayoutData(gridData2Columns2);

		TableColumn column1 = new TableColumn(table, SWT.LEFT);
		TableColumn column2 = new TableColumn(table, SWT.RIGHT);

		final TableColumn[] columns = table.getColumns();
		// for (int i = 0; i < 1; i++) {
		TableItem item = new TableItem(table, SWT.NONE);

		item.setText(0, "Item");
		item.setText(1, "123 KB");
		item.setForeground(1, Display.getDefault().getSystemColor(SWT.COLOR_GRAY));

		// }
		for (TableColumn column : columns) {
			column.pack();
		}
		table.remove(0);
		l.setText(table.getItemCount() + " embedded files");

		// final DropTarget dropTarget = new DropTarget(table, DND.DROP_MOVE);
		// dropTarget.setTransfer(new Transfer[] { FileTransfer.getInstance() });
		// dropTarget.addDropListener(new DropTargetAdapter() {
		//
		// @Override
		// public void drop(final DropTargetEvent event) {
		// final String[] filenames = (String[]) event.data;
		// final Path p = Paths.get(filenames[0]);
		// String file = p.getFileName().toString();
		// TableItem item = new TableItem(table, SWT.NONE);
		// item.setText(file);
		// }
		// });

		// Text t2 = new Text(parent, SWT.SINGLE | SWT.BORDER);
		// t2.setText(L.getString("file_text"));
		// t2.setEnabled(false);
		// t2.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false));
		//
		// Button b = new Button(parent, SWT.PUSH);
		// b.setLayoutData(new GridData(SWT.RIGHT, SWT.BEGINNING, false, false));
		// b.setToolTipText(L.getString("file_button_tooltip"));
		// b.setText(L.getString("file_button"));
		return composite;
	}

	Text createPasswordField(Composite parent) {

		GridData data = new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1);

		// Label labelKey = new Label(g, SWT.LEFT);
		// labelKey.setText("Secret key:");
		// labelKey.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false));

		Text t2 = new Text(parent, SWT.SINGLE | SWT.BORDER);
		t2.setText(L.getString("key_text"));
		t2.setForeground(Display.getDefault().getSystemColor(SWT.COLOR_DARK_GRAY));
		t2.setToolTipText(L.getString("key_text_tooltip"));
		t2.setLayoutData(data);
		t2.addListener(SWT.MouseDown, event -> {
			if (t2.getData() == null) {
				t2.setData(1);
				t2.setForeground(null);
				t2.setText("");
			}
		});

		t2.addListener(SWT.FocusOut, event -> {
			if (t2.getText().isEmpty()) {
				t2.setText(L.getString("key_text"));
				t2.setForeground(Display.getDefault().getSystemColor(SWT.COLOR_DARK_GRAY));
				t2.setData(null);
			}
		});

		// GridLayout mGridLayoutEncryption = new GridLayout(1, true);
		// mGridLayoutEncryption.verticalSpacing = 6;
		// mGridLayoutEncryption.marginWidth = 0;
		// mGridLayoutEncryption.marginHeight = 0;
		// Composite compositeEncryption = new Composite(g, SWT.NONE);
		// compositeEncryption.setLayoutData(data);
		// compositeEncryption.setLayout(mGridLayoutEncryption);

		// Button checkBox1 = new Button(compositeEncryption, SWT.CHECK);
		// checkBox1.setText("Encryption");
		// checkBox1.setToolTipText("Encrypt the payload using AES with a secret key.");
		//
		// Text t3 = new Text(compositeEncryption, SWT.SINGLE | SWT.BORDER);
		// t3.setEnabled(false);
		// t3.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false));
		// t3.addListener(SWT.MouseDown, event -> {
		// t3.selectAll();
		// });
		//
		// checkBox1.addListener(SWT.Selection, event -> {
		// t3.setEnabled(checkBox1.getSelection());
		// if (checkBox1.getSelection()) {
		// t3.setText("secret-message-key");
		// } else {
		// t3.setText("");
		// }
		// });

		return t2;
	}

	Button createMainButton(Composite parent, String text) {
		Button b = new Button(parent, SWT.PUSH);
		b.setText(text);
		GridData gd = new GridData();
		gd.widthHint = (b.computeSize(SWT.DEFAULT, SWT.DEFAULT).x) * 2;
		b.setLayoutData(gd);
		return b;
	}

	Composite createImageComposite(Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);
		GridLayout gridLayout = new GridLayout(1, false);
		gridLayout.marginRight = 12;
		gridLayout.marginLeft = 12;
		gridLayout.marginHeight = 17;
		gridLayout.verticalSpacing = 6;
		gridLayout.horizontalSpacing = 0;
		composite.setLayout(gridLayout);
		return composite;
	}

	TabItem createTabItem(TabFolder tabFolder, String name) {
		TabItem tabItem = new TabItem(tabFolder, SWT.NONE);
		tabItem.setText(name);

		Composite c1 = new Composite(tabFolder, SWT.NONE);
		c1.setLayout(new FillLayout());
		tabItem.setControl(c1);
		return tabItem;
	}
}
