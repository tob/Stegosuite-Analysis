package org.stegosuite.analysis.ui.gui;

import java.util.Arrays;
import java.util.ResourceBundle;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.ImageLoader;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.stegosuite.analysis.image.analysis.LsbVisualization;
import org.stegosuite.analysis.image.analysis.SortedColorTableAttack;
import org.stegosuite.analysis.image.format.BMPImage;
import org.stegosuite.analysis.image.format.GIFImage;
import org.stegosuite.analysis.image.format.ImageFormat;
import org.stegosuite.analysis.image.util.FileUtils;
import org.stegosuite.analysis.ui.gui.ImageContainer.ImageState;
import org.swtchart.Chart;

/**
 * Contains the GUI of the TabItem "Analyze", which contains several means for steganalysis of
 * images.
 */
public class AnalyzeUi {

	// private TabItem tabItem;
	private Composite compositeImage;
	private ImageFormat image;
	private ImageContainer imageContainer;
	private Label label;
	private Button analyzeButton, radioButtonLSBVisualization, radioButtonChiSquare, radioButtonPoVHist,
			radioButtonNeighbourhoodHist, radioButtonDifferenceImageHist, radioButtonColorTableVisualization,
			radioButtonUnreferencedColor;
	private Chart currentChart;
	private final ResourceBundle L = ResourceBundle.getBundle("Messages");

	public AnalyzeUi(Composite composite, GuiComponents myComponents) {
		// tabItem = new TabItem(tabFolder, SWT.NONE);
		// tabItem.setText(L.getString("analyze_tab"));

		composite.setLayout(new FillLayout());
		// tabItem.setControl(c1);

		Composite compositeControls = new Composite(composite, SWT.NONE);
		RowLayout mRowLayout = new RowLayout(SWT.VERTICAL);
		mRowLayout.spacing = 18;
		mRowLayout.marginTop = 12;
		mRowLayout.marginLeft = 12;
		compositeControls.setLayout(mRowLayout);

		Group g = new Group(compositeControls, SWT.NONE);
		g.setText(L.getString("options_group"));
		g.setLayout(new RowLayout(SWT.VERTICAL));

		radioButtonLSBVisualization = new Button(g, SWT.RADIO);
		radioButtonLSBVisualization.setText("LSB-Visualization");
		radioButtonChiSquare = new Button(g, SWT.RADIO);
		radioButtonChiSquare.setText("Chi-Square");
		radioButtonPoVHist = new Button(g, SWT.RADIO);
		radioButtonPoVHist.setText("PoV histogramm");
		radioButtonNeighbourhoodHist = new Button(g, SWT.RADIO);
		radioButtonNeighbourhoodHist.setText("Neighbourhood histogram");
		radioButtonDifferenceImageHist = new Button(g, SWT.RADIO);
		radioButtonDifferenceImageHist.setText("Pixel difference histogram");
		radioButtonColorTableVisualization = new Button(g, SWT.RADIO);
		radioButtonColorTableVisualization.setText("GIF Sorted Color Table-Visualization");
		radioButtonUnreferencedColor = new Button(g, SWT.RADIO);
		radioButtonUnreferencedColor.setText("Check for unreferenced colors");

		// analyzeButton = myComponents.createMainButton(compositeControls,
		// L.getString("analyze_button"));
		analyzeButton = new Button(compositeControls, SWT.PUSH);
		analyzeButton.setText(L.getString("analyze_button"));

		// tabItem.addListener(SWT.Selection, event -> {
		// if (label != null && !label.isDisposed() && label.getImage() != null) {
		// label.setImage(imageContainer.scaleImage());
		// compositeImage.layout(true, true);
		// Gui.setStatusBarMsg("");
		// }
		// });
		//
		composite.addListener(SWT.Resize, event -> {
			if (label != null && !label.isDisposed() && label.getImage() != null) {
				label.setImage(imageContainer.scaleImage());
				compositeImage.layout(true, true);
			}
		});

		analyzeButton.addListener(SWT.Selection, event -> {
			clearCompositeImage();
			if (radioButtonLSBVisualization.getSelection()) {
				currentChart = null;
				if (label.isDisposed()) {
					label = new Label(compositeImage, SWT.NONE);
				}
				label.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false));
				imageContainer.setImageData(ImageState.STEG_LSB, LsbVisualization.calculate(image).getImageData());
				label.setImage(imageContainer.scaleImage(ImageState.STEG_LSB));
			} else if (radioButtonChiSquare.getSelection()) {
				// Label labelChiSquare = new Label(compositeImage, SWT.NONE);
				// labelChiSquare.setLayoutData(new GridData(SWT.CENTER,
				// SWT.BEGINNING, false, false));
				currentChart = ImageUtils.getChiSquarePropability(compositeImage, image);
				// labelChiSquare.setText(String.format("%.5f", chi));
			} else if (radioButtonPoVHist.getSelection()) {
				currentChart = ImageUtils.showPovHistogram(compositeImage, image);
			} else if (radioButtonNeighbourhoodHist.getSelection()) {
				currentChart = ImageUtils.showNeighbourhoodHistogram(compositeImage, image);
			} else if (radioButtonDifferenceImageHist.getSelection()) {
				currentChart = ImageUtils.showPixelsDifferenceHistogram(compositeImage, image);
			} else if (radioButtonColorTableVisualization.getSelection()) {
				currentChart = null;
				if (label.isDisposed()) {
					label = new Label(compositeImage, SWT.NONE);
					label.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false));
				}
				imageContainer.setImageData(ImageState.STEG_LSB2,
						SortedColorTableAttack.calculate(image).getImageData());
				label.setImage(imageContainer.scaleImage(ImageState.STEG_LSB2));
			} else if (radioButtonUnreferencedColor.getSelection()) {
				MessageBox dialog = new MessageBox(composite.getShell(), SWT.ICON_INFORMATION | SWT.OK);
				dialog.setText("Unreferenced Colors");
				int unrefColors = ImageUtils.getUnreferencedColors(image);
				String message = "There are " + Integer.toString(unrefColors) + " unreferenced colors in the palette.";
				dialog.setMessage(message);
				dialog.open();
			}

			compositeImage.layout(true, true);
		});

		compositeImage = myComponents.createImageComposite(composite);

		// compositeImage.setBackground(tabFolder.getDisplay().getSystemColor(SWT.COLOR_RED));
	}

	/**
	 * Loads an image from the given path, displays it and displays the according options.
	 *
	 * @param image The image to load
	 */
	void loadImage(ImageFormat image) {
		this.image = image;
		imageContainer = new ImageContainer(compositeImage);
		if (label == null || label.isDisposed()) {
			clearCompositeImage();
			label = new Label(compositeImage, SWT.NONE);
		}
		label.setImage(imageContainer.loadImage(image.getImageData()));
		label.setToolTipText(image.getFile().getAbsolutePath());
		if (image.getClass().equals(GIFImage.class)) {
			radioButtonNeighbourhoodHist.setEnabled(false);
			radioButtonColorTableVisualization.setEnabled(true);
			radioButtonUnreferencedColor.setEnabled(true);
			radioButtonDifferenceImageHist.setEnabled(false);
			radioButtonChiSquare.setEnabled(false);
		} else if (image.getClass().equals(BMPImage.class)) {
			radioButtonNeighbourhoodHist.setEnabled(true);
			radioButtonColorTableVisualization.setEnabled(false);
			radioButtonUnreferencedColor.setEnabled(false);
			radioButtonDifferenceImageHist.setEnabled(true);
			radioButtonChiSquare.setEnabled(true);
		}
		compositeImage.layout(true, true);
	}

	private void clearCompositeImage() {
		if (compositeImage.getChildren().length > 0) {
			for (Control c : compositeImage.getChildren()) {
				c.dispose();
			}
		}
	}

	void saveDiagram(final String path) {
		String outputPath;
		if (path != null) {
			outputPath = path;
		} else {
			outputPath = FileUtils.addFileNameSuffix(image.getFile().getAbsolutePath(), "_analyzed");
		}
		if (currentChart == null) {
			if (imageContainer.getImageData(ImageState.STEG_LSB) == null) {
				return;
			}
			if (Arrays.asList(ImageState.STEG_LSB, ImageState.STEG_LSB2).contains(imageContainer.getState())) {
				ImageData imageData = imageContainer.getImageData(imageContainer.getState());
				saveImageDataToFile(imageData, outputPath);
			}
		} else {
			Point size = currentChart.getSize();
			GC gc = new GC(currentChart);
			Image image = new Image(Display.getDefault(), size.x, size.y);
			gc.copyArea(image, 0, 0);
			gc.dispose();
			saveImageDataToFile(image.getImageData(), outputPath);
			image.dispose();
		}
		Gui.setStatusBarMsg("Diagram saved to " + outputPath);
	}

	private void saveImageDataToFile(ImageData imageData, String outputPath) {
		ImageLoader loader = new ImageLoader();
		loader.data = new ImageData[] { imageData };
		loader.save(outputPath, SWT.IMAGE_PNG);
	}
}
