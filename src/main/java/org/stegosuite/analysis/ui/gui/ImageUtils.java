package org.stegosuite.analysis.ui.gui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.stegosuite.analysis.image.analysis.ChiSquare;
import org.stegosuite.analysis.image.analysis.NeighbourhoodHistogram;
import org.stegosuite.analysis.image.analysis.PixelsDifferenceHistogram;
import org.stegosuite.analysis.image.analysis.PovHistogram;
import org.stegosuite.analysis.image.format.GIFImage;
import org.stegosuite.analysis.image.format.ImageFormat;
import org.stegosuite.analysis.image.util.ColorUtils;
import org.swtchart.Chart;
import org.swtchart.IBarSeries;
import org.swtchart.ILineSeries;
import org.swtchart.ILineSeries.PlotSymbolType;
import org.swtchart.ISeries.SeriesType;
import org.swtchart.Range;

public class ImageUtils {

	// taken from http://stackoverflow.com/a/24805871
	static String formatSize(long v) {
		if (v < 1024) {
			return v + " B";
		}
		int z = (63 - Long.numberOfLeadingZeros(v)) / 10;
		return String.format("%.1f %sB", (double) v / (1L << (z * 10)), " KMGTPE".charAt(z));
	}

	static int getUnreferencedColors(ImageFormat image) {
		GIFImage gifImage = (GIFImage) image;
		return ColorUtils.getUnreferencedColors(gifImage.getColorTable(), gifImage.getPixels()).size();
	}

	static Chart getChiSquarePropability(Composite composite, ImageFormat image) {
		// create a chart
		Chart chart = new Chart(composite, SWT.NONE);

		// set titles
		chart.getTitle().setText("Chi-Square-Distribution");
		chart.getAxisSet().getXAxis(0).getTitle().setText("Data Points");
		chart.getAxisSet().getYAxis(0).getTitle().setText("Propability");

		// create line series
		ILineSeries lineSeries = (ILineSeries) chart.getSeriesSet().createSeries(SeriesType.LINE, "Chi-Square");
		lineSeries.setYSeries(ChiSquare.chiSquareAttackTopToBottom(image));
		lineSeries.setSymbolType(PlotSymbolType.NONE);
		lineSeries.setLineColor(Display.getDefault().getSystemColor(SWT.COLOR_RED));
		// adjust the axis range
		chart.getAxisSet().adjustRange();
		chart.getAxisSet().getYAxes()[0].setRange(new Range(0, 1));

		chart.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		return chart;
	}

	static Chart showPixelsDifferenceHistogram(Composite parent, ImageFormat image) {
		// create a chart
		Chart chart = new Chart(parent, SWT.NONE);

		// set titles
		chart.getTitle().setText("Image difference histogram");
		chart.getAxisSet().getXAxis(0).getTitle().setText("Values");
		chart.getAxisSet().getYAxis(0).getTitle().setText("Frequency");

		// create bar series
		double[] histogram = PixelsDifferenceHistogram.attack(image);
		double[] histogram2 = new double[81];
		for (int i = 0; i < histogram2.length; i++) {
			histogram2[i] = histogram[i + 215];
		}
		IBarSeries barSeries1 = (IBarSeries) chart.getSeriesSet().createSeries(SeriesType.BAR, "legend");
		barSeries1.setBarPadding(100);
		barSeries1.setBarColor(Display.getDefault().getSystemColor(SWT.COLOR_RED));
		barSeries1.setYSeries(histogram2);

		/*
		 * double[] xSeries = new double[511]; for (int i = 0; i < xSeries.length; i++) { xSeries[i]
		 * = i - 255; }
		 */
		double[] xSeries = new double[81];
		for (int i = 0; i < xSeries.length; i++) {
			xSeries[i] = i - 40;
		}

		barSeries1.setXSeries(xSeries);

		// Adjust range
		chart.getAxisSet().adjustRange();
		Range r = chart.getAxisSet().getYAxis(0).getRange();
		r.upper += (r.upper * 0.01);
		chart.getAxisSet().getYAxis(0).setRange(r);

		// Hide legend
		chart.getLegend().setVisible(false);

		chart.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		return chart;
	}

	static Chart showPovHistogram(Composite parent, ImageFormat image) {
		PovHistogram.attack(image);

		// create a chart
		Chart chart = new Chart(parent, SWT.NONE);

		// set titles
		chart.getTitle().setText("Pair of Values");
		chart.getAxisSet().getXAxis(0).getTitle().setText("\nAdjacent values");
		chart.getAxisSet().getYAxis(0).getTitle().setText("Frequency");

		// set category
		chart.getAxisSet().getXAxis(0).enableCategory(true);
		chart.getAxisSet().getXAxis(0).setCategorySeries(new String[] { "0000\n0001", "0010\n0011", "0100\n0101",
				"0110\n0111", "1000\n1001", "1010\n1011", "1100\n1101", "1110\n1111" });

		// create bar series
		IBarSeries barSeries1 = (IBarSeries) chart.getSeriesSet().createSeries(SeriesType.BAR, "LSB = 0");
		barSeries1.setYSeries(PovHistogram.getLsbZero());
		barSeries1.setBarColor(Display.getDefault().getSystemColor(SWT.COLOR_BLUE));

		IBarSeries barSeries2 = (IBarSeries) chart.getSeriesSet().createSeries(SeriesType.BAR, "Mean");
		barSeries2.setYSeries(PovHistogram.getMeans());
		barSeries2.setBarColor(Display.getDefault().getSystemColor(SWT.COLOR_YELLOW));

		IBarSeries barSeries3 = (IBarSeries) chart.getSeriesSet().createSeries(SeriesType.BAR, "LSB = 1");
		barSeries3.setYSeries(PovHistogram.getLsbOne());
		barSeries3.setBarColor(Display.getDefault().getSystemColor(SWT.COLOR_RED));

		// adjust the axis range
		chart.getAxisSet().adjustRange();
		chart.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		return chart;
	}

	static Chart showNeighbourhoodHistogram(Composite parent, ImageFormat image) {
		double[] ySeries = NeighbourhoodHistogram.attack(image);
		// create a chart
		Chart chart = new Chart(parent, SWT.NONE);

		// set titles
		chart.getTitle().setText("Neighbourhood Histogram\nMean = " + NeighbourhoodHistogram.getMean(ySeries));
		chart.getAxisSet().getXAxis(0).getTitle().setText("Frequency");
		chart.getAxisSet().getYAxis(0).getTitle().setText("Neighbours");

		/*
		 * Color black = Display.getDefault().getSystemColor(SWT.COLOR_BLACK);
		 * chart.getAxisSet().getXAxis(0).getTick().setForeground(black);
		 * chart.getAxisSet().getYAxis(0).getTick().setForeground(black);
		 * chart.getTitle().setForeground(black);
		 * chart.getAxisSet().getXAxis(0).getTitle().setForeground(black);
		 * chart.getAxisSet().getYAxis(0).getTitle().setForeground(black);
		 */

		// create bar series
		IBarSeries barSeries = (IBarSeries) chart.getSeriesSet().createSeries(SeriesType.BAR, "legend");
		barSeries.setYSeries(ySeries);
		barSeries.setBarColor(Display.getDefault().getSystemColor(SWT.COLOR_RED));

		// adjust the axis range
		chart.getAxisSet().adjustRange();
		Range r = chart.getAxisSet().getYAxis(0).getRange();
		r.upper += (r.upper * 0.03);
		chart.getAxisSet().getYAxis(0).setRange(r);
		chart.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		// Hide the useless legend
		chart.getLegend().setVisible(false);

		return chart;
	}
}
